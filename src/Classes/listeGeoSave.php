<?php
function getGeoSave($database, $listeGeoSave_id){
    $s = "SELECT * FROM listeGeoSave WHERE listeGeoSaveId = ?";
    $stmt = $database->prepare($s);
    $stmt->execute(array($listeGeoSave_id));
    $listeRecherche = $stmt->fetch();
    return $listeRecherche;
}

class listeGeoSave{
    private $_listeGeoSave_id;
    private $_ville;
    private $_lat;
    private $_lng;

    public function __construct($database, $listeGeoSave_id){
        $this->_listeGeoSave_id = $listeGeoSave_id;
        $list = getGeoSave($database, $listeGeoSave_id);
        //print_r($list);
        $this->_ville = $list["ville"];
        $this->_lat = $list["lat"];
        $this->_lng = $list["lng"];
    }

    public function get__listeGeoSave_id(){
        return $this->_listeGeoSave_id;
    }
    public function get_ville(){
        return $this->_ville;
    }
    public function get_lat(){
        return $this->_lat;
    }
    public function get_lng(){
        return $this->_lng;
    }
    
}