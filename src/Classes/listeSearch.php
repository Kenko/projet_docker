<?php
function getListe($database, $liste_id){
    $s = "SELECT * FROM listeSearch WHERE liste_id = ?";
    $stmt = $database->prepare($s);
    $stmt->execute(array($liste_id));
    $listeRecherche = $stmt->fetch();
    return $listeRecherche;
}

class listeSearch{
    private $_liste_id;
    private $_from;
    private $_to;
    private $_distance;
    private $_duration;

    public function __construct($database, $liste_id){
        $this->_liste_id = $liste_id;
        $list = getListe($database, $liste_id);
        //print_r($list);
        $this->_from = $list["from"];
        $this->_to = $list["to"];
        $this->_distance = $list["distance"];
        $this->_duration = $list["duration"];
    }

    public function get_liste_id(){
        return $this->_liste_id;
    }
    public function get_from(){
        return $this->_from;
    }
    public function get_to(){
        return $this->_to;
    }
    public function get_distance(){
        return $this->_distance;
    }
    public function get_duration(){
        return $this->_duration;
    }
    
}