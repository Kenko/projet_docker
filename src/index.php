﻿<?php 
require_once 'db/connect.php';
require 'Classes/listeSearch.php'; 
require 'Classes/listeGeoSave.php'; 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Distances btn two cities App</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://kit.fontawesome.com/ab2155e76b.js" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="App.css" rel="stylesheet" />
</head>
<body>
  
    <?php 
    $connection = new DbConnect();
    $database = $connection->connect();
	?>
    <div class="jumbotron">
        <div class="container-fluid">
            <h1>Find The Distance Between Two Places.</h1>
            <p>This App Will Help You Calculate Your Travelling Distances.</p>
            <form class="form-horizontal" action="script/add.php" method="post">
                <div class="form-group">
                    <label for="from" class="col-xs-2 control-label"><i class="far fa-dot-circle"></i></label>
                    <div class="col-xs-4">
                        <input type="text" id="from" name="from" placeholder="Origin" class="form-control">
                    </div>
               </div>
               <div class="form-group">
                
                    <label for="to" class="col-xs-2 control-label"><i class="fas fa-map-marker-alt"></i></label>
                    <div class="col-xs-4">
                        <input type="text" id="to" name="to" placeholder="Destination" class="form-control">
                    </div>
                  <button type="submit" class="btn btn-info btn-lg ">SAVE DB</button>
                 </div>
                 
                 <div id="value-to-send">

                 </div>
                </form>
                 <div class="col-xs-offset-2 col-xs-10">
                     <button type="submit" class="btn btn-info btn-lg " onclick="calcRoute();"><i class="fas fa-map-signs"></i></button>
                    </div>
        </div>
        <div class="container-fluid">
            <div id="googleMap">

            </div>
            <div id="output">
                
            </div>
            <div class="container">
                    <table class="table">
                        <tr>
                            <th colspan="9" class="text-center" > Save Reseach </th>
                        </tr>
                        <tr>
                            <?php 
                                $head = array("FROM","TO","DISTANCE","DURATION");
                                foreach($head as $h){
                                ?>
                                    <th>
                                    <?= $h ?>
                                    </th>
                                <?php
                                }
                            ?>
                            
                        </tr>
                        <?php
                            $liste_id = array();
                            $q = "SELECT liste_id FROM listeSearch";
                            $query = $database->query($q);
                            $ids = $query->fetchAll();
                            foreach($ids as $id){
                                array_push($liste_id, $id['liste_id']);
                            }
                            //print_r($liste_id);
                            foreach($liste_id as $id){
                                $liste = new listeSearch($database, $id);
                                ?>
                            <tr>
                                <td>
                                    <?= $liste->get_from() ?>
                            </td>
                                <td>
                                    <?= $liste->get_to() ?>
                            </td>
                                <td>
                                    <?= $liste->get_distance() ?>
                            </td>
                                <td>
                                    <?= $liste->get_duration() ?>
                            </td>
                            </tr>
                            <?php
                            }
                            ?>
                    </table>
                    <table class="table">
                        <tr>
                            <th colspan="9" class="text-center" > Location Save </th>
                        </tr>
                        <tr>
                            <?php 
                                $head = array("VILLE","LONGITUDE","LATITUDE");
                                foreach($head as $h){
                                ?>
                                    <th>
                                    <?= $h ?>
                                    </th>
                                <?php
                                }
                            ?>
                            
                        </tr>
                            <?php 
                                $q = "SELECT * FROM listeGeoSave";
                                $query = $database->query($q);
                                $allData = $query->fetchAll();
                                $all = json_encode($allData, true);
                                //print_r($allData2);
                                echo '<div id="locationSave" hidden>' . $all . '</div>';

                                $geoId = array();
                                $q = "SELECT listeGeoSaveId FROM listeGeoSave";
                                $query = $database->query($q);
                                $ids = $query->fetchAll();
                                foreach($ids as $id){
                                    array_push($geoId, $id['listeGeoSaveId']);
                                }
                                foreach($geoId as $gId){
                                    $GeoSave = new listeGeoSave($database, $gId);
                                    
                                    ?>
                                <tr>
                                    <td>
                                        <?= $GeoSave->get_ville(); ?>
                                    </td>
                                    <td>
                                        <?= $GeoSave->get_lng(); ?>
                                    </td>
                                    <td>
                                        <?= $GeoSave->get_lat(); ?>
                                    </td>
        
                                </tr>
                                <?php
                            }
                            ?>
                    </table>
                </div>
        </div>

    </div>
    <?php 
        
    ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeOMcGG8NxbNM48X6Te1RLu5gMnRC21Ek&libraries=places"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Scripts/jquery-3.1.1.min.js"></script>
    <script src="js/Main.js"></script>
</body>
</html>
