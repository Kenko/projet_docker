-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql_db
-- Généré le : mer. 29 juin 2022 à 12:39
-- Version du serveur : 8.0.29
-- Version de PHP : 8.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `google_docker`
--

-- --------------------------------------------------------

--
-- Structure de la table `listeGeoSave`
--

CREATE TABLE `listeGeoSave` (
  `listeGeoSaveId` int NOT NULL,
  `ville` varchar(255) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `listeGeoSave`
--

INSERT INTO `listeGeoSave` (`listeGeoSaveId`, `ville`, `lat`, `lng`) VALUES
(1, 'Paris, France', 2.347035, 48.858856),
(2, 'Tours, France', 0.694943, 47.394268);

-- --------------------------------------------------------

--
-- Structure de la table `listeShearch`
--

CREATE TABLE `listeSearch` (
  `liste_id` int NOT NULL,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `distance` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `listeShearch`
--

INSERT INTO `listeSearch` (`liste_id`, `from`, `to`, `distance`, `duration`) VALUES
(2, 'Paris, France', 'Montargis, France', '126 km', '1 heure 31 min'),
(3, 'Paris, France', 'Montpellier, France', '748 km', '7 heures 14 minutes'),
(4, 'Montargis, France', 'Tours, France', '210 km', '2 heures 1 minute');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `listeGeoSave`
--
ALTER TABLE `listeGeoSave`
  ADD PRIMARY KEY (`listeGeoSaveId`);

--
-- Index pour la table `listeSearch`
--
ALTER TABLE `listeSearch`
  ADD PRIMARY KEY (`liste_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
