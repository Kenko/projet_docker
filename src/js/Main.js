﻿//javascript.js
//set map options
navigator.geolocation.getCurrentPosition(showPosition);
var directionsService;
var directionsDisplay;

let map;
let marker;
let geocoder;
let responseDiv;
let response;
let form;
//create a DirectionsService object to use the route method and get a result for our request
directionsService = new google.maps.DirectionsService();

//create a DirectionsRenderer object which we will use to display the route
directionsDisplay = new google.maps.DirectionsRenderer();
async function showPosition(position) {


    var pos = { lat: position.coords.latitude, lng: position.coords.longitude };
    var mapOptions = {
        center: pos,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    };

    //create map
    map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);

    geocoder = new google.maps.Geocoder();

    const inputText = document.createElement("input");

    inputText.type = "text";
    inputText.placeholder = "Enter a location";

    const submitButton = document.createElement("input");

    submitButton.type = "button";
    submitButton.value = "Geocode";
    submitButton.classList.add("button", "button-primary");

    const clearButton = document.createElement("input");

    clearButton.type = "button";
    clearButton.value = "Clear";
    clearButton.classList.add("button", "button-secondary");
    /* response = document.createElement("pre");
    response.id = "response";
    response.innerText = ""; */
    form = document.createElement('form');
    form.action = "script/geoSave.php";
    form.method = "post";

    responseDiv = document.createElement("div");
    responseDiv.id = "response-container";
    responseDiv.appendChild(form); 

    const instructionsElement = document.createElement("p");

    instructionsElement.id = "instructions";
    /* instructionsElement.innerHTML =
        "<strong>Instructions</strong>: Enter an address in the textbox to geocode or click on the map to reverse geocode."; */
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(inputText);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(submitButton);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(clearButton);
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(instructionsElement);
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(responseDiv);
    marker = new google.maps.Marker({
        map,
    });
    map.addListener("click", (e) => {
        geocode({ location: e.latLng });
    });
    submitButton.addEventListener("click", () =>
        geocode({ address: inputText.value })
    );
    clearButton.addEventListener("click", () => {
        clear();
    });
    clear();

    let allLocation = JSON.parse(document.getElementById('locationSave').innerHTML);
    showLocationSave(allLocation);

    //bind the DirectionsRenderer to the map
    directionsDisplay.setMap(map);
}



//define calcRoute function
function calcRoute() {
    //create request
    var request = {
        origin: document.getElementById("from").value,
        destination: document.getElementById("to").value,
        travelMode: google.maps.TravelMode.DRIVING, //WALKING, BYCYCLING, TRANSIT
        unitSystem: google.maps.UnitSystem.METRIC
    }


    //pass the request to the route method
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {

            //Get distance and time
            const output = document.querySelector('#output');
            output.innerHTML = "<div class='alert-info'>From: " + document.getElementById("from").value + ".<br />To: " + document.getElementById("to").value + ".<br /> Driving distance <i class='fas fa-road'></i> : " + result.routes[0].legs[0].distance.text + ".<br />Duration <i class='fas fa-hourglass-start'></i> : " + result.routes[0].legs[0].duration.text + ".</div>";
            
            let DivForadd = document.getElementById('value-to-send');
            let inputDistance = document.createElement('input');
            inputDistance.name = "distance_selected";
            inputDistance.value = result.routes[0].legs[0].distance.text;
            inputDistance.hidden = true;
            DivForadd.appendChild(inputDistance);
            
            let inputDuration = document.createElement('input');
            inputDuration.name = "duration_selected";
            inputDuration.value = result.routes[0].legs[0].duration.text;
            inputDuration.hidden = true;
            DivForadd.appendChild(inputDuration);
            //display route
            directionsDisplay.setDirections(result);
        } else {
            //delete route from map
            directionsDisplay.setDirections({ routes: [] });
            //center map in London
            map.setCenter(myLatLng);

            //show error message
            output.innerHTML = "<div class='alert-danger'><i class='fas fa-exclamation-triangle'></i> Could not retrieve driving distance.</div>";
        }
        console.log(result.routes[0].legs[0].distance);
    });

}

function clear() {
    marker.setMap(null);
    responseDiv.style.display = "none";
}

function geocode(request) {
    clear();
    geocoder
        .geocode(request)
        .then((result) => {
            const { results } = result;

            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
            marker.setMap(map);
            responseDiv.style.display = "block";
            //response.innerText = JSON.stringify(result, null, 2);
            let table = document.createElement('table');
            let row = table.insertRow();
            row.id = 'ville';
            row.innerHTML = "<span style='color= black;'>Ville rechercher : </span>" + result["results"][0]["formatted_address"];

            let inputVille = document.createElement('input');
            inputVille.name = "ville_selected";
            inputVille.value = result["results"][0]["formatted_address"];
            inputVille.hidden = true;
            table.appendChild(inputVille);
            
            let rows = table.insertRow();
            rows.id = 'location';
            rows.innerHTML = "location : " + (result["results"][0]["geometry"]["bounds"]["ub"]["hi"] + result["results"][0]["geometry"]["bounds"]["ub"]["lo"])/2 + " , " + (result["results"][0]["geometry"]["bounds"]["Ra"]["hi"] + result["results"][0]["geometry"]["bounds"]["Ra"]["lo"])/2;
            
            let inputlat = document.createElement('input');
            inputlat.name = "lat";
            inputlat.value = (result["results"][0]["geometry"]["bounds"]["Ra"]["hi"] + result["results"][0]["geometry"]["bounds"]["Ra"]["lo"])/2;
            inputlat.hidden = true;
            table.appendChild(inputlat);
            
            let inputlng = document.createElement('input');
            inputlng.name = "lng";
            inputlng.value = (result["results"][0]["geometry"]["bounds"]["ub"]["hi"] + result["results"][0]["geometry"]["bounds"]["ub"]["lo"])/2;
            inputlng.hidden = true;
            table.appendChild(inputlng);

            table.appendChild(row);
            table.appendChild(rows); 

            let button = document.createElement('button');
            button.type = "submit";
            //button.className = "form-control";
            button.textContent = "ADD LIST";
            form.appendChild(button);
            form.appendChild(table);
            console.log(result["results"][0]["geometry"]["bounds"]);
            return results;
        })
        .catch((e) => {
            alert("Geocode was not successful for the following reason: " + e);
        });
}

//create autocomplete objects for all inputs
var options = {
    types: ['(cities)']
}

var input1 = document.getElementById("from");
var autocomplete1 = new google.maps.places.Autocomplete(input1, options);

var input2 = document.getElementById("to");
var autocomplete2 = new google.maps.places.Autocomplete(input2, options);

function showLocationSave(allData) {
    var infoWind = new google.maps.InfoWindow;
    Array.prototype.forEach.call(allData, function(data) {
        var content = document.createElement('div');
        var strong = document.createElement('strong');

        strong.textContent = data.ville;
        content.appendChild(strong);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(data.lng, data.lat),
            map: map
        });

        marker.addListener('mouseover', function() {
            infoWind.setContent(content);
            infoWind.open(map, marker);
        })
    })
}
