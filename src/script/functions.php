<?php
function getMaxId($database, $table, $field){
    $s = "SELECT max(" . $field . ") as max FROM " . $table;
    $stmt = $database->prepare($s);
    $stmt->execute();
    $r = $stmt->fetch();
    return $r["max"] + 1;
}