<?php
require_once "../db/connect.php";
require_once "functions.php";
$fields = $_POST;

    $connection = new DbConnect();
    $database = $connection->connect();

if(isset($fields['ville_selected']) && isset($fields['lat']) && isset($fields['lng'])){
    $ville = $fields['ville_selected'];
    $lat = $fields['lat'];
    $lng = $fields['lng'];

    $id = getMaxId($database, "listeGeoSave", "listeGeoSaveId");
    $s = "INSERT INTO listeGeoSave VALUES (?, ?, ?, ?)";
    $stmt= $database->prepare($s);
    $stmt->execute(array($id, $ville, $lat, $lng));
}
header('Location: ../');