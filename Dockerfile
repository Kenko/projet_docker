FROM php:8.0-apache
WORKDIR /var/www/html
RUN apt-get update -y && apt-get install -y libmariadb-dev
RUN docker-php-ext-install pdo pdo_mysql
#COPY src/backupImage/google_docker.sql .
#RUN exec cMySql /bin/sh 'mysql -u root -proot < ./src/backupImage/google_docker.sql'

# FROM mysql/mysql-server:5.7
# 
# ENV MYSQL_ALLOW_EMPTY_PASSWORD=true
# ENV MYSQL_USER=root
# ENV MYSQL_PASSWORD=root
# ENV MYSQL_DATABASE=google_docker
# 
# COPY src/backupImage/ /docker-entrypoint-initdb.d/
# 
# COPY src/backupImage/google_docker.sql .
# RUN cMySql mysql -u root -p root CREATE DATABASE google_docker
# RUN mysql -u root -p root google_docker < src/backupImage/google_docker.sql
EXPOSE 80