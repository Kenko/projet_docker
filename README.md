## projet_docker OUANNA MATHIAS - BOUMALI LIZA

Le but de ce projet etait de réaliser une application dockeriser

Dans notre projet nous avons réaliser un systeme de recherche de trajet qui nous permet donc de voir la distance et le temps de parcours.

Pour ce faire nous avons utilisé une connection a l'api google maps ainsi que c'est divers module réglable de par le google cloud platform for develloper

## pour lancer le projet:

ce placer a la racine du projet dans votre terminal
lancer la commande : docker-compose --compatibility up -d

sur votre navigateur :
 1 - initialise database:
   phpMyadmin :
    rentrer l'url : localhost:9001 
    server: mysql_db
    username: root
    password: root

    create database: 
        créer base de donnée : nom: google_docker encode: utf8_general_ci
        import le fichier google_docker.sql se trouvant dans ../src/backupImage/

 2 - lancer le projet
    rentrer l'url: localhost:9000
